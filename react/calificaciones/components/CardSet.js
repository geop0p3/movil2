import React from "react";
import PropTypes from "prop-types";
import { ScrollView, Text, View, StyleSheet } from "react-native";
import Card from "./Card";

const CardSet = ({ items }) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        {items.map((item, i) => (
          <View key={i} style={styles.cardstyle}>
            <Card {...item} />
          </View>
        ))}
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: "#F5F5F5"
  },
  cardstyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 16
  }
});

CardSet.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      ciudad: PropTypes.string.isRequired,
      fisica: PropTypes.string.isRequired,
      profesorfisica: PropTypes.string.isRequired,
      mate: PropTypes.string.isRequired,
      profesormate: PropTypes.string.isRequired,
      geografia: PropTypes.string.isRequired,
      profesorgeografia: PropTypes.string.isRequired
    })
  ).isRequired
};
export default CardSet;

