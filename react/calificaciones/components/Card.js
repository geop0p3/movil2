import React from "react";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  Linking
} from "react-native";
import PropTypes from "prop-types";
const Card = ({ title, image, ciudad, fisica,profesorfisica,mate,profesormate,geografia,profesorgeografia }) => {
  const imageSource = {
    uri: image
  };
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.containerImg}>
        <Image style={styles.img2} source={imageSource} />
      </View>
      <Text style={styles.subtitle}>{ciudad}</Text>
      
      <View style={{flex: 2, flexDirection: 'row', marginBottom:8}}>
      <Text style={styles.description}>{fisica}</Text>
      <Text style={styles.profesor}>Profesor: {profesorfisica}</Text>
      </View>
      
      <View style={{flex: 2, flexDirection: 'row',marginBottom:8, backgroundColor:'#F5F5F5'}}>
      <Text style={styles.description}>{mate}</Text>
      <Text style={styles.profesor}>Profesor: {profesormate}</Text>
      </View>
      
      <View style={{flex: 2, flexDirection: 'row',marginBottom:8}}>
      <Text style={styles.description}>{geografia}</Text>
      <Text style={styles.profesor}>Profesor: {profesorfisica}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    backgroundColor: "#ffffff",
    padding: 10,
    justifyContent: "center",
    borderRadius: 5
  },
  title: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: '800',
    marginBottom:8
  },
  containerImg: {
    alignItems: "center",
    justifyContent: "center"
  },

  img: {
    height: 150,
    width: 150,
    borderRadius: 150/2
  },
  img2: {
    width: 200,
    height: 200,
    borderRadius: 200/2,
    borderColor: '#fafafa',
    borderWidth: 6
  },
  description: {
    fontSize: 14,
    color: "#212121",
    textAlign: "center",
    marginLeft: 10,
    marginTop: 20,
    marginRight: 10,
    fontWeight: '100',
    flex:1
  },
  button: {
    height: 30,
    marginTop: 10,
    backgroundColor: "#000",
    width: 140,
    alignSelf: "flex-end",
    borderRadius: 5
  },
  buttonText: {
    color: "#fff",
    lineHeight: 38,
    textAlign: "center",
    fontWeight: "bold"
  },
  subtitle: {
    fontSize: 18,
    textAlign: "center",
    fontWeight: '500',
    marginBottom:8,
    marginTop:8
  },
  profesor: {
      fontSize: 12,
      color: "#212121",
      textAlign: "center",
      marginLeft: 10,
      marginTop: 20,
      marginRight: 10,
      fontWeight: '100',
      flex:1
  }
  
});

Card.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  ciudad: PropTypes.string.isRequired,
  fisica: PropTypes.string.isRequired,
  profesorfisica: PropTypes.string.isRequired,
  mate: PropTypes.string.isRequired,
  profesormate: PropTypes.string.isRequired,
  geografia: PropTypes.string.isRequired,
  profesorgeografia: PropTypes.string.isRequired
  
};

export default Card;

