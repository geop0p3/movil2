const alumnos = [{
	"id": 1,
	"title": "Carlos Flores",
	"image": "https:\/\/i.ytimg.com\/vi\/x_HL0wiK4Zc\/maxresdefault.jpg",
	"ciudad": "Tijuana",
	"fisica": "Fisica: 8",
	"profesorfisica": "Ruben Robles",
	"mate": "Mate: 10",
	"profesormate": "Javier Lopez",
	"geografia": "Geografia: 6",
	"profesorgeografia": "John Lenon"
}, {
	"id": 2,
	"title": "Tomas Delgado",
	"image": "https:\/\/cdn.thedailymash.co.uk\/wp-content\/uploads\/20190324205212\/middle-aged-man-fat-2.jpg",
	"ciudad": "Mexicali",
	"fisica": "Fisica: 8",
	"profesorfisica": "Ruben Robles",
	"mate": "Mate: 10",
	"profesormate": "Javier Lopez",
	"geografia": "Geografia: 6",
	"profesorgeografia": "John Lenon"
}, {
	"id": 3,
	"title": "Guillermo Monjardin",
	"image": "https:\/\/cdn.themodestman.com\/wp-content\/uploads\/2018\/01\/fp1-mobile-4.jpg",
	"ciudad": "Guadalajara",
	"fisica": "Fisica: 8",
	"profesorfisica": "Ruben Robles",
	"mate": "Mate: 10",
	"profesormate": "Javier Lopez",
	"geografia": "Geografia: 6",
	"profesorgeografia": "John Lenon"
}, {
	"id": 4,
	"title": "Victor Martinez",
	"image": "https:\/\/cbsnews1.cbsistatic.com\/hub\/i\/2018\/11\/06\/0c1af1b8-155a-458e-b105-78f1e7344bf4\/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg",
	"ciudad": "San Luis",
	"fisica": "Fisica: 8",
	"profesorfisica": "Ruben Robles",
	"mate": "Mate: 10",
	"profesormate": "Javier Lopez",
	"geografia": "Geografia: 6",
	"profesorgeografia": "John Lenon"
}]
export default alumnos;
