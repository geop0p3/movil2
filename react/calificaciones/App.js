import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import CardSet from "./components/CardSet";
import alumnos from "./alumnos";

import { Constants } from "expo";


export default class App extends React.Component {

  
  render() {
    let component = <CardSet items={alumnos} />;
    return <View style={styles.container}>{component}</View>;
  }
  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight
  }
});

const styleText = StyleSheet.create({
  container: {
    color: "#fff",
    backgroundColor: "#000"
  }
});

