import React from 'react';
import { Button } from 'react-native';

const Boton = props => {
	return (
		<Button title="Concatenar" onPress={props.onConcat} />
	);
};

export default Boton;
