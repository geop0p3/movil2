import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Boton from './componentes/Boton';

export default class App extends React.Component {
  
  state = {
      nombrecompleto : "Aqui aparece el nombre"
  }
  
  concatenar = () => {
      this.setState ({
         nombrecompleto: this.props.nombre + " " + this.props.apellido
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> {this.state.nombrecompleto}</Text>
        <Boton onConcat={this.concatenar} />
      </View>
    );
  }
}


App.defaultProps={
    nombre: 'Carlos',
    apellido: 'Flores',
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
