import React from 'react';
import ReactNative, {View, Text, StyleSheet} from 'react-native';

const Colores = () => {
    return (
        <View style={{flex: 1}}>
            <View style={{flex: 2, marginTop: '50%' }}>
                <Text style={styles.texto}>Ejercicio 1</Text>
            </View>
            <View style={{flex: 2, flexDirection: 'row'}}>
                <View style={{flex:1, backgroundColor: '#f44336'}} />
                <View style={{flex:1, backgroundColor: '#4CAF50'}} />
                <View style={{flex:1, backgroundColor: '#2196F3'}} />
            </View>
            <View style={{flex: 3, backgroundColor: '#90A4AE'}} />
        </View>
    )
};

const styles = StyleSheet.create({
    texto: {
      color: '#212121',
      backgroundColor: '#ffffff',
      textAlign: 'center',
      fontSize: 48
    },
});

export default Colores
