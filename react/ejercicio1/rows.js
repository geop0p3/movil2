import React from 'react';
import ReactNative, {View, Text, StyleSheet} from 'react-native';

const Colores = () => {
    return (
        <View style={{flex: 1}}>
            <View style={{flex: 2, marginTop: '45%' }}>
                <Text style={styles.texto}>Ejercicio 1</Text>
            </View>
            <View style={{flex: 2, flexDirection: 'row'}}>
                <View style={{flex:1, backgroundColor: 'red'}} />
                <View style={{flex:1, backgroundColor: 'green'}} />
                <View style={{flex:1, backgroundColor: 'blue'}} />
            </View>
            <View style={{flex: 3, backgroundColor: 'steelblue'}} />
        </View>
    )
};

const styles = StyleSheet.create({
    texto: {
      color: '#212121',
      backgroundColor: '#ffffff',
      textAlign: 'center',
      size: 28px
    },
});

export default Colores
