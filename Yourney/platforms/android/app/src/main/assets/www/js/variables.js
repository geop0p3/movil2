var $$ = new DisplayJS(window);

var meses = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre"
];

var dia = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miercoles",
  "Jueves",
  "Viernes",
  "Sabado"
];

var now = new Date();
var numerodia = now.getDay();
var hora = now.getHours();
var numeromes = now.getDate();
var mes = now.getMonth();
var minutos = ("0" + now.getMinutes()).slice(-2);
var diahoy = dia[numerodia];

var dia4 = dia[numerodia - 1];
var numerodia4 = numerodia - 1;

var dia3 = dia[numerodia - 2];
var numerodia3 = numerodia - 2;

var dia2 = dia[numerodia - 3];
var numerodia2 = numerodia - 3;

var dia1 = dia[numerodia - 4];
var numerodia1 = numerodia - 4;

var metanueva=null;
var titulonueva = null;
var textonueva = null;
var imgnueva = null;

var entradas =[];
var entrada = { fecha: metanueva, texto: textonueva, titulo: titulonueva, imagen: imgentrada };

var metaentrada;
var textoentrada;
var tituloentrada;
var imgentrada;

var cerrado = true;

function takephoto() {
  let take = {
    quality: 80,
    destinationType: Camera.DestinationType.FILE_URI,
    sourceType: Camera.PictureSourceType.CAMERA,
    mediaType: Camera.MediaType.PICTURE,
    encodingType: Camera.EncodingType.JPEG,
    cameraDirection: Camera.Direction.BACK,
    targetWidth: 300,
    targetHeight: 400,
    correctOrientation: true  //Corrects Android orientation quirks
  };

  navigator.camera.getPicture(success, error, take);
}

function success(imgURI) {
  $("#dummyimage").attr("src", imgURI);
  imgnueva = imgURI;
}
function error(msg) {
  console.log(msg);
}

function setdate() {
  var numerodiaentrada = now.getDay();
  var horaentrada = now.getHours();
  var numeromesentrada = now.getDate();
  var mesentrada = now.getMonth();
  var minutosentrada = ("0" + now.getMinutes()).slice(-2);
  var fechastringentrada =
    dia[numerodia] + " " + numeromes + " de " + meses[mes];
  var tiempoentrada = horaentrada + ":" + minutosentrada;
  metanueva = tiempoentrada + ", " + fechastringentrada;
}

