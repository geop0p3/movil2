import * as React from "react";
import {
  StyleSheet,
  Image,
  Alert,
  Linking,
  Keyboard,
  Dimensions,
  ScrollView,
  View,
  ActivityIndicator
} from "react-native";
import { Button, Layout, Text, Input } from "react-native-ui-kitten";
import Icon from "../Images/reportar.png";
import Foto from "../Images/photo.png";
import image from "../Images/image.png";
import check from "../Images/check.png";
import firebase from "../Firebase";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

export default class Reportar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Raza: "",
      Color: "",
      Direccion: "",
      Comentario: "",
      photo: null,
      comentarioStatus: "",
      colorStatus: "",
      direccionStatus: "",
      comentarioStatus: "",
      razaStatus: "",
      photoStatus: "filled",
      photoTextTake: "Tomar Foto",
      photoTextPick: "Elegir Foto",
      fotoIconTake: this.photoIcon,
      fotoIconPick: this.imageIcon,
      photoState: "",
      keyboardOffset: 0,
      showPhoto: false,
      photoUrl: "",
      Loading: false
    };

    this.ref = firebase.firestore().collection("reportes");
  }

  componentDidMount() {
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    this.state.screenHeight = Dimensions.get("window");
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    // Save the content height in state
    this.setState({ screenHeight: contentHeight });
  };

  componentWillUnmount() {
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = event => {
    const screenHeight = Math.round(Dimensions.get("window").height);
    this.setState({
      keyboardOffset: screenHeight * 0.2
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardOffset: 0
    });
  };

  onRazaChange = (Raza: string) => {
    this.setState({ Raza });
    if (Raza.length >= 4) {
      this.setState({ razaStatus: "success" });
    } else {
      this.setState({ razaStatus: "danger" });
    }
  };

  onColorChange = (Color: string) => {
    this.setState({ Color });

    if (Color.length >= 4) {
      this.setState({ colorStatus: "success" });
    } else {
      this.setState({ colorStatus: "danger" });
    }
  };

  onDireccionChange = (Direccion: string) => {
    this.setState({ Direccion });
    if (Direccion.length >= 10) {
      this.setState({ direccionStatus: "success" });
    } else {
      this.setState({ direccionStatus: "danger" });
    }
  };

  onComentarioChange = (Comentario: string) => {
    this.setState({ Comentario });
    this.setState({ comentarioStatus: "success" });
  };

  async checkCameraRollPermission() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      Alert.alert("Hey", "Necesitamos permiso para acceder a tus fotos", [
        {
          text: "Abrir Configuración",
          onPress: () => Linking.openURL("app-settings:")
        },
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
      this.setState({
        hasCameraRollPermissions: false
      });
      return false;
    }
    this.setState({
      hasCameraRollPermissions: true
    });
    return true;
  }

  async checkCameraPermission() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status !== "granted") {
      Alert.alert("Hey", "Necesitamos permiso para acceder a tus fotos", [
        {
          text: "Abrir Configuración",
          onPress: () => Linking.openURL("app-settings:")
        },
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
      this.setState({
        hasCameraPermissions: false
      });
      return false;
    }
    this.setState({
      hasCameraPermissions: true
    });
    return true;
  }

  photoIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={Foto} />;
  };

  imageIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={image} />;
  };

  checkIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={check} />;
  };

  takePhoto = async () => {
    const checkPermissions = await this.checkCameraPermission();

    if (!checkPermissions) return;

    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      quality: 0.2
    });

    if (!result.cancelled) {
      this.setState({
        photo: result.uri,
        photoStatus: "success",
        photoState: "outline",
        fotoIconTake: this.checkIcon,
        fotoIconPick: this.imageIcon,
        photoTextTake: "Reemplazar Foto",
        photoTextPick: "Reemplazar Foto",
        showPhoto: true
      });
    }
  };

  pickPhoto = async () => {
    const checkPermissions = await this.checkCameraRollPermission();

    if (!checkPermissions) return;

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.2
    });

    if (!result.cancelled) {
      this.setState({
        photo: result.uri,
        photoStatus: "success",
        photoState: "outline",
        fotoIconPick: this.checkIcon,
        fotoIconTake: this.photoIcon,
        photoTextPick: "Reemplazar Foto",
        photoTextMake: "Reemplazar Foto",
        showPhoto: true
      });
    }
  };

  uploadImage = async (uri, imageName) => {
    console.log("uploading");
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase
      .storage()
      .ref()
      .child(firebase.auth().currentUser.uid + "/" + imageName);

    ref.put(blob).then(result => {
      ref.getDownloadURL().then(result => {
        this.setState({ photoUrl: result });
        this.ref
          .add({
            raza: this.state.Raza,
            color: this.state.Color,
            direccion: this.state.Direccion,
            photo: this.state.photoUrl,
            comentario: this.state.Comentario,
            userid: firebase.auth().currentUser.uid,
            time: new Date()
          })
          .then(docRef => {
            Alert.alert("¡Reportado!", "Gracias por tu Ayuda", [
              { text: "OK", onPress: () => console.log("OK ButtonPressed") }
            ]);
            this.setState({
              Raza: "",
              Color: "",
              Direccion: "",
              Comentario: "",
              photo: null,
              photoTextPick: "Elegir Imagen",
              photoTextTake: "Tomar Foto",
              photoStatus: "filled",
              razaStatus: "",
              colorStatus: "",
              direccionStatus: "",
              photoState: "",
              fotoIconTake: this.photoIcon,
              fotoIconPick: this.imageIcon,
              showPhoto: false,
              photoUrl: null,
              Loading: false
            });
          })
          .catch(error => {
            const { code, message } = error;
            Alert.alert("¡Error!", message, [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      });
    });
  };

  reportar = () => {
    if (
      this.state.razaStatus == "success" &&
      this.state.colorStatus == "success" &&
      this.state.direccionStatus == "success" &&
      this.state.photo != null
    ) {
      this.setState({
        Loading: true
      });
      const imageName =
        "Photo" +
        Math.floor(Math.random() * 1000) +
        1 +
        Math.floor(Math.random() * 1000) +
        1;
      this.uploadImage(this.state.photo, imageName);
    } else {
      Alert.alert("¡Error!", "Asegurate de llenar los campos correctamente", [
        { text: "OK", onPress: () => console.log("Hwa") }
      ]);
    }
  };

  render() {
    const estilo = {
      bottom: this.state.keyboardOffset,
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    };
    return (
      <ScrollView
        style={styles.scroll}
        onContentSizeChange={this.onContentSizeChange}
      >
        <Layout style={estilo}>
          <Image source={Icon} style={styles.logo} resizeMode="center" />
          <Text style={styles.text} category="h5">
            Reportar
          </Text>
          <Input
            style={styles.input}
            value={this.state.Raza}
            onChangeText={this.onRazaChange}
            placeholder="Raza"
            status={this.state.razaStatus}
          />
          <Input
            style={styles.input}
            value={this.state.Color}
            onChangeText={this.onColorChange}
            placeholder="Color"
            status={this.state.colorStatus}
          />
          <Input
            style={styles.input}
            value={this.state.Direccion}
            onChangeText={this.onDireccionChange}
            placeholder="Dirección Donde Lo Viste"
            status={this.state.direccionStatus}
          />
          <Input
            style={styles.input}
            value={this.state.Comentario}
            onChangeText={this.onComentarioChange}
            placeholder="Comentario Extra"
            onFocus={this._keyboardDidShow}
          />
          <Button
            status={this.state.photoStatus}
            onPress={this.takePhoto}
            icon={this.state.fotoIconTake}
            style={styles.boton}
            appearance={this.state.photoState}
          >
            {this.state.photoTextTake}
          </Button>

          <Button
            status={this.state.photoStatus}
            onPress={this.pickPhoto}
            icon={this.state.fotoIconPick}
            style={styles.boton}
            appearance={this.state.photoState}
          >
            {this.state.photoTextPick}
          </Button>

          {this.state.showPhoto && (
            <Image
              source={{ uri: this.state.photo }}
              style={{
                width: 120,
                height: 120,
                marginVertical: 8
              }}
            />
          )}
          <Button
            disabled={!this.state.Direccion.length}
            onPress={this.reportar}
            style={styles.boton}
            status="success"
          >
            ¡Reportar!
          </Button>

          {this.state.Loading && (
            <View style={styles.dim}>
              <Text style={styles.blanco} category="h5">
                Subiendo reporte...
              </Text>
              <ActivityIndicator
                style={styles.loading}
                size="large"
                color="#00DF99"
              />
            </View>
          )}
        </Layout>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 4
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 50
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  scroll: {
    flexGrow: 1,
    marginVertical: 64
  },
  content: {
    alignItems: "center",
    alignSelf: "stretch"
  },
  loading: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  dim: {
    backgroundColor: "#21212160",
    height: Dimensions.get("window").height,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  blanco: {
    marginVertical: 4,
    color: "#ffffff"
  }
});

