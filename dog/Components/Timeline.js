import * as React from "react";
import {
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  View,
  ActivityIndicator
} from "react-native";
import { Button, Layout, Text } from "react-native-ui-kitten";
import firebase from "../Firebase";
import Icon from "../Images/lupa.png";
import Card from "./Card";
import { List, ListItem } from "react-native-elements";

export default class Timeline extends React.Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection("reportes").orderBy('time','desc');
    this.unsubscribe = null;
    this.state = {
      Loading: true,
      Reportes: [],
      Empty: true
    };
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onCollectionUpdate = querySnapshot => {
    const Reportes = [];
    querySnapshot.forEach(doc => {
      const { color, comentario, direccion, photo, raza, userid } = doc.data();
      Reportes.push({
        key: doc.id,
        raza,
        direccion,
        color,
        comentario,
        photo,
        userid
      });
    });
    this.setState({
      Reportes,
      Loading: false
    });

    if (this.state.Reportes && this.state.Reportes.length > 0) {
      
      this.setState({
      Empty: false
    });
    }
  };

  render() {
    return (
      <ScrollView
        style={styles.scroll}
        onContentSizeChange={this.onContentSizeChange}
      >
        <Layout style={styles.container}>
          <Image source={Icon} style={styles.logo} resizeMode="center" />
          <Text style={styles.text} category="h5">
            Reportes
          </Text> 
          {this.state.Empty && (
            <Text style={styles.text} category="h1">
              Sin Reportes 😿
            </Text>
          )}
          {this.state.Reportes.map((l, i) => (
            <View style={styles.carta} key={i}>
              <Card
                Raza={l.raza}
                Foto={l.photo}
                Color={l.color}
                Userid={l.userid}
                Direccion={l.direccion}
                Comentario={l.comentario}
              />
            </View>
          ))}

          {this.state.Loading && (
            <ActivityIndicator
              style={styles.loading}
              size="large"
              color="#00DF99"
            />
          )}
        </Layout>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 50
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  loading: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    marginVertical: 16
  },
  dim: {
    flex: 1,
    backgroundColor: "#21212160",
    height: Dimensions.get("window").height,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    paddingVertical: Dimensions.get("window").height
  },
  blanco: {
    marginVertical: 4,
    color: "#ffffff"
  },
  carta: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.15,
    shadowRadius: 5,
    elevation: 3,
    alignSelf: "stretch",
    marginHorizontal: 16,
    marginVertical: 24,
    paddingHorizontal: 24,
    paddingVertical: 24,
    borderRadius: 8
  },
  scroll: {
    flexGrow: 1,
    marginVertical: 64
  }
});

