import * as React from "react";
import { StyleSheet, Image, Linking, Platform, View } from "react-native";
import {
  Button,
  Layout,
  Text,
  Avatar,
  AvatarProps
} from "react-native-ui-kitten";
import Icon from "../Images/lupa.png";
import firebase from "../Firebase";
import phone from "../Images/phone.png";

export default class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: "",
      profilePicture: "https://hedronmx.com/avatar.jpeg",
      telefono:""
    };

    this.ref = firebase
      .firestore()
      .collection("usuarios")
      .doc(this.props.Userid);
  }

  componentDidMount() {
    this.ref
      .get()
      .then(doc => {
        if (!doc.exists) {
        } else {
          this.setState({
            displayName: doc.data().nombre,
            profilePicture: doc.data().photo,
            telefono: doc.data().telefono
          });
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
    if (this.props.Comentario != "") {
      this.setState({
        comentario: this.props.Comentario
      });
    }
  }
  
  dialCall = (number) => {
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else {phoneNumber = `telprompt:${number}`; }
    Linking.openURL(phoneNumber);
 };
 
 phoneIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={phone} />;
  };

  render() {
    return (
      <Layout>
        <Image
          source={{ uri: this.props.Foto }}
          style={styles.logo}
          resizeMode="center"
        />
        <Text style={styles.text}>Raza: {this.props.Raza}</Text>
        <Text style={styles.text}>Color: {this.props.Color}</Text>
        <Text style={styles.text}>
          Ultima vez visto en: {this.props.Direccion}
        </Text>
        {this.state.comentario && (
          <Text style={styles.text}>Comentario: {this.state.comentario}</Text>
        )}
        <View style={styles.split}>
          <Avatar size="giant" source={{ uri: this.state.profilePicture }} />
          <Text style={styles.texto}> {this.state.displayName}</Text>
        </View>
        <Button icon={this.phoneIcon} style={styles.boton} onPress={()=>{this.dialCall(this.state.telefono)}} status="info">
          Contactar
        </Button>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 8,
    paddingHorizontal: 8
  },
  text: {
    marginVertical: 8
  },
   texto: {
    marginVertical: 8,
    marginHorizontal: 12
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 180
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  split: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  }
});

