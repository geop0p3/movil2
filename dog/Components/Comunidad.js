import * as React from "react";
import {
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  View,
  ActivityIndicator
} from "react-native";
import { Button, Layout, Text, Input } from "react-native-ui-kitten";
import firebase from "../Firebase";
import Icono from "../Images/comunidad.png";
import Card from "./SocialCard";
import { List, ListItem } from "react-native-elements";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import image from "../Images/image.png";
import check from "../Images/check.png";
import Foto from "../Images/photo.png";

export default class Comunidad extends React.Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection("publicaciones");
    this.unsubscribe = null;
    this.state = {
      Loading: true,
      Reportes: [],
      Empty: true,
      fotoIconTake: this.photoIcon,
      fotoIconPick: this.imageIcon,
      photoState: "ghost",
      showPhoto: false,
      photoUrl: "",
      photo: null,
      photoStatus: "",
      status: "",
      Comentario: ""
    };
  }

  componentDidMount() {
    this.unsubscribe = this.ref.orderBy('time','desc').onSnapshot(this.onCollectionUpdate);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onCollectionUpdate = querySnapshot => {
    const Reportes = [];
    querySnapshot.forEach(doc => {
      const { comentario, photo, userid  } = doc.data();
      Reportes.push({
        key: doc.id,
        comentario,
        photo,
        userid
      });
    });
    this.setState({
      Reportes,
      Loading: false
    });

    if (this.state.Reportes && this.state.Reportes.length > 0) {

      this.setState({
      Empty: false
    });
    }
  };
  
  onComentarioChange = (Comentario: string) => {
    this.setState({ Comentario });
  };


  async checkCameraRollPermission() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      Alert.alert("Hey", "Necesitamos permiso para acceder a tus fotos", [
        {
          text: "Abrir Configuración",
          onPress: () => Linking.openURL("app-settings:")
        },
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
      this.setState({
        hasCameraRollPermissions: false
      });
      return false;
    }
    this.setState({
      hasCameraRollPermissions: true
    });
    return true;
  }

  async checkCameraPermission() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status !== "granted") {
      Alert.alert("Hey", "Necesitamos permiso para acceder a tus fotos", [
        {
          text: "Abrir Configuración",
          onPress: () => Linking.openURL("app-settings:")
        },
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
      this.setState({
        hasCameraPermissions: false
      });
      return false;
    }
    this.setState({
      hasCameraPermissions: true
    });
    return true;
  }

  photoIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={Foto} />;
  };

  imageIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={image} />;
  };

  checkIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={check} />;
  };


  takePhoto = async () => {
    const checkPermissions = await this.checkCameraPermission();

    if (!checkPermissions) return;

    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      quality: 0.2
    });

    if (!result.cancelled) {
      this.setState({
        photo: result.uri,
        photoStatus: "success",
        photoState: "outline",
        fotoIconTake: this.checkIcon,
        fotoIconPick: this.imageIcon,
        photoTextTake: "Reemplazar Foto",
        photoTextPick: "Reemplazar Foto",
        showPhoto: true
      });
    }
  };

  pickPhoto = async () => {
    const checkPermissions = await this.checkCameraRollPermission();

    if (!checkPermissions) return;

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.2
    });

    if (!result.cancelled) {
      this.setState({
        photo: result.uri,
        photoStatus: "success",
        photoState: "outline",
        fotoIconPick: this.checkIcon,
        fotoIconTake: this.photoIcon,
        showPhoto: true
      });
    }
  };

  uploadImage = async (uri, imageName) => {
    
    if (this.state.photo != null){
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase
      .storage()
      .ref()
      .child(firebase.auth().currentUser.uid + "/" + imageName);
      
      ref.put(blob).then(result => {
      ref.getDownloadURL().then(result => {
        this.setState({ photoUrl: result });
        this.ref
          .add({
            photo: this.state.photoUrl,
            comentario: this.state.Comentario,
            userid: firebase.auth().currentUser.uid,
            time: new Date()
          })
          .then(docRef => {
            this.setState({
              Comentario: "",
              photo: "null",
              fotoIconTake: this.photoIcon,
              fotoIconPick: this.imageIcon,
              showPhoto: false,
              photoUrl: null,
              photoState: "ghost",
              photoStatus: ""
            });
          })
          .catch(error => {
            const { code, message } = error;
            Alert.alert("¡Error!", message, [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      });
    });
    } else {
      this.ref
          .add({
            photo: "",
            comentario: this.state.Comentario,
            userid: firebase.auth().currentUser.uid,
            time: new Date()
          })
          .then(docRef => {
            this.setState({
              Comentario: "",
              photo: "null",
              fotoIconTake: this.photoIcon,
              fotoIconPick: this.imageIcon,
              showPhoto: false,
              photoUrl: null,
              photoState: "ghost",
              photoStatus: ""
            });
          })
          .catch(error => {
            const { code, message } = error;
            Alert.alert("¡Error!", message, [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
    }
    

    
  };

  reportar = () => {
      const imageName =
        "Photo" +
        Math.floor(Math.random() * 1000) +
        1 +
        Math.floor(Math.random() * 1000) +
        1;
      this.uploadImage(this.state.photo, imageName);
  };

  render() {
    return (
      <ScrollView
        style={styles.scroll}
        onContentSizeChange={this.onContentSizeChange}
      >
        <Layout style={styles.container}>
          <Image source={Icono} style={styles.logo} resizeMode="center" />
          <Text style={styles.text} category="h5">
            Comunidad
          </Text>
          <View>
          <Input
            style={styles.input}
            value={this.state.Comentario}
            onChangeText={this.onComentarioChange}
            placeholder="Qué vas a publicar?"
          />
          {this.state.showPhoto && (
            <Image
              source={{ uri: this.state.photo }}
              style={{
                width: 120,
                height: 120,
                marginVertical: 8
              }}
            />
          )}
           <Button
            status={this.state.photoStatus}
            onPress={this.takePhoto}
            icon={this.state.fotoIconTake}
            style={styles.boton}
            appearance={this.state.photoState}
          >
          </Button>

          <Button
            status={this.state.photoStatus}
            onPress={this.pickPhoto}
            icon={this.state.fotoIconPick}
            style={styles.boton}
            appearance={this.state.photoState}
          >
          </Button>
          <Button
            onPress={this.reportar}
            icon={this.checkIcon}
            style={styles.boton}
            disabled={!this.state.Comentario.length}
          > Publicar
          </Button>
          </View>
          {this.state.Empty && (
            <Text style={styles.text} category="h1">
              Sin Publicaciones 🐶️
            </Text>
          )}
          {this.state.Reportes.map((l, i) => (
            <View style={styles.carta} key={i}>
              <Card
                Foto={l.photo}
                Userid={l.userid}
                Comentario={l.comentario}
              />
            </View>
          ))}

          {this.state.Loading && (
            <ActivityIndicator
              style={styles.loading}
              size="large"
              color="#00DF99"
            />
          )}
        </Layout>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 50
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  loading: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    marginVertical: 16
  },
  dim: {
    flex: 1,
    backgroundColor: "#21212160",
    height: Dimensions.get("window").height,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    paddingVertical: Dimensions.get("window").height
  },
  blanco: {
    marginVertical: 4,
    color: "#ffffff"
  },
  carta: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.15,
    shadowRadius: 5,
    elevation: 3,
    alignSelf: "stretch",
    marginHorizontal: 16,
    marginVertical: 24,
    paddingHorizontal: 24,
    paddingVertical: 24,
    borderRadius: 8
  },
  scroll: {
    flexGrow: 1,
    marginVertical: 64
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  }
});

