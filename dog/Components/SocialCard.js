import * as React from "react";
import { StyleSheet, Image, Linking, Platform, View } from "react-native";
import {
  Button,
  Layout,
  Text,
  Avatar,
  AvatarProps
} from "react-native-ui-kitten";
import firebase from "../Firebase";
import phone from "../Images/phone.png";

export default class SocialCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: "",
      profilePicture: "https://hedronmx.com/avatar.jpeg",
    };

    this.ref = firebase
      .firestore()
      .collection("usuarios")
      .doc(this.props.Userid);
  }

  componentDidMount() {
    this.ref
      .get()
      .then(doc => {
        if (!doc.exists) {
        } else {
          this.setState({
            displayName: doc.data().nombre,
            profilePicture: doc.data().photo,
          });
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
    if (this.props.Foto != "") {
      this.setState({
        photo: this.props.Foto
      });
    }
  }
  

  render() {
    return (
      <Layout>
      {this.state.photo && (
          <Image
          source={{ uri: this.state.photo }}
          style={styles.logo}
          resizeMode="center"
        />
        )}
        <Text style={styles.text}>{this.props.Comentario}</Text>
        <View style={styles.split}>
          <Avatar size="giant" source={{ uri: this.state.profilePicture }} />
          <Text style={styles.texto}> {this.state.displayName}</Text>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 8,
    paddingHorizontal: 8
  },
  text: {
    marginVertical: 8
  },
   texto: {
    marginVertical: 8,
    marginHorizontal: 12
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 180
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  split: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  }
});

