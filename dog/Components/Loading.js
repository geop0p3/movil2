import * as React from "react";
import { StyleSheet, ActivityIndicator, Image } from "react-native";
import { Layout, Text } from "react-native-ui-kitten";
import Logo from "../Images/logo.png";

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Layout style={styles.container}>
        <Image source={Logo} style={styles.logo} resizeMode="center" />
        <Text style={styles.text} category="h1">
          Iniciando Sesión{" "}
        </Text>
        <Text style={styles.text} category="h6">
          {" "}
          😺 ¡Es bueno verte de nuevo! 🐶{" "}
        </Text>
        <ActivityIndicator size="large" color="#1867F9" />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 100
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  }
});

