import * as React from "react";
import {
  StyleSheet,
  Image,
  Alert,
  BackHandler,
  ImageProps,
  Keyboard,
  Dimensions
} from "react-native";
import {
  Button,
  Layout,
  Text,
  Input,
  TopNavigation,
  TopNavigationAction,
  TopNavigationActionProps
} from "react-native-ui-kitten";
import Logo from "../Images/logo.png";
import back from "../Images/back.png";
import firebase from "../Firebase";

export default class Recover extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Correo: "",
      correoStatus: "",
      correoCaption: "",
      disabled: true
    };
    this.Sure = this.Sure.bind(this);
    this.Recover = this.Recover.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = event => {
    const screenHeight = Math.round(Dimensions.get("window").height);
    this.setState({
      keyboardOffset: screenHeight * 0.2
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardOffset: 0
    });
  };

  handleBackButton = () => {
    this.props.navigation.navigate("Login");
    return true;
  };

  Sure = navigate => {
    Alert.alert("¡Atención!", "Deseas Reestablecer Tu Contraseña?", [
      {
        text: "CANCELAR",
        onPress: () => console.log("OK ButtonPressed"),
        style: "cancel"
      },
      { text: "OK", onPress: () => this.Recover(navigate) }
    ]);
  };

  onCorreo = (Correo: string) => {
    this.setState({ Correo });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(Correo) === true) {
      this.setState({ correoStatus: "success" });
      this.setState({ correoCaption: "" });
      this.setState({ disabled: false });
    } else {
      this.setState({ correoStatus: "danger" });
      this.setState({ correoCaption: "Correo electrónico incorrecto" });
      this.setState({ disabled: true });
    }
  };

  Recover = navigate => {
    firebase
      .auth()
      .sendPasswordResetEmail(this.state.Correo)
      .then(function(user) {
        Alert.alert("¡Listo!", "Recibiras un correo con mayor informacion", [
          { text: "OK", onPress: () => navigate("Login") }
        ]);
      })
      .catch(function(e) {
        console.log(e);
        const { code, message } = e;
        Alert.alert("Error codigo: " + code, message, [
          { text: "OK", onPress: () => navigate("Login") }
        ]);
      });
  };

  renderControlIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={back} />;
  };

  renderLeftControl = (): React.ReactElement<TopNavigationActionProps> => {
    return (
      <TopNavigationAction
        icon={this.renderControlIcon}
        onPress={this.handleBackButton}
      />
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    const estilo = {
      bottom: this.state.keyboardOffset,
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    };

    return (
      <Layout style={estilo}>
        <TopNavigation
          style={styles.abajo}
          leftControl={this.renderLeftControl()}
        />
        <Image source={Logo} style={styles.logo} resizeMode="center" />
        <Text style={styles.text} category="h1">
          Perritos
        </Text>
        <Text style={styles.textito}>
          Ingresa tu Correo Electrónico Para Reestablecer tu Contraseña
        </Text>
        <Input
          style={styles.input}
          value={this.state.Correo}
          onChangeText={this.onCorreo}
          placeholder="Correo Electrónico"
          autoCorrect={false}
          autoCapitalize="none"
          status={this.state.correoStatus}
          caption={this.state.correoCaption}
          keyboardType="email-address"
          onFocus={this._keyboardDidShow}
        />
        <Button
          onPress={() => this.Sure(navigate)}
          disabled={this.state.disabled}
          status="danger"
          style={styles.boton}
        >
          Restablecer Contraseña
        </Button>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  textito: {
    marginHorizontal: 8,
    textAlign: "center"
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 100
  },
  back: {
    height: 32
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  abajo: {
    position: "absolute",
    top: 0,
    marginVertical: 32
  }
});

