import * as React from "react";
import {
  StyleSheet,
  Image,
  BackHandler,
  Alert,
  Keyboard,
  Dimensions
} from "react-native";
import {
  Button,
  Layout,
  Text,
  Input,
  TopNavigation,
  TopNavigationAction,
  TopNavigationActionProps
} from "react-native-ui-kitten";
import Logo from "../Images/logo.png";
import firebase from "../Firebase";
import back from "../Images/back.png";

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Nombre: "",
      Usuario: "",
      Correo: "",
      Telefono: "",
      Contrasena: "",
      nombreStatus: "",
      telefonoStatus: "",
      telefonoCaption: "",
      correoStatus: "",
      correoCaption: "",
      contrasenaStatus: "",
      contrasenaCaption: "",
      keyboardOffset: 0
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = event => {
    const screenHeight = Math.round(Dimensions.get("window").height);
    this.setState({
      keyboardOffset: screenHeight * 0.2
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardOffset: 0
    });
  };

  handleBackButton = () => {
    this.props.navigation.navigate("Login");
    return true;
  };

  onNombre = (Nombre: string) => {
    this.setState({ Nombre });
    this.setState({ nombreStatus: "success" });
  };

  onCorreo = (Correo: string) => {
    this.setState({ Correo });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(Correo) === true) {
      this.setState({ correoStatus: "success" });
      this.setState({ correoCaption: "" });
    } else {
      this.setState({ correoStatus: "danger" });
      this.setState({ correoCaption: "Correo electrónico incorrecto" });
    }
  };

  onTelefono = (Telefono: string) => {
    let reg = /^[0]?[789]\d{9}$/;
    this.setState({ Telefono });
    if (reg.test(Telefono) === false && Telefono.length == 10) {
      this.setState({ telefonoStatus: "success" });
      this.setState({ telefonoCaption: "" });
    } else {
      this.setState({ telefonoStatus: "danger" });
      this.setState({ telefonoCaption: "Número de teléfono incorrecto" });
    }
  };

  onContrasena = (Contrasena: string) => {
    this.setState({ Contrasena });
    if (Contrasena.length >= 6) {
      this.setState({ contrasenaStatus: "success" });
      this.setState({ contrasenaCaption: "" });
    } else {
      this.setState({ contrasenaStatus: "danger" });
      this.setState({
        contrasenaCaption: "La contraseña debe contener al menos 6 caracteres"
      });
    }
  };

  Register = () => {
    if (
      this.state.contrasenaStatus == "success" &&
      this.state.correoStatus == "success" &&
      this.state.nombreStatus == "success" &&
      this.state.telefonoStatus == "success"
    ) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(
          this.state.Correo,
          this.state.Contrasena
        )
        .then(user => {
          console.log("usuario: ", user);
          user.user.updateProfile({
            displayName: this.state.Nombre
          });
          console.log(user.user.uid);
          const ref = firebase
            .firestore()
            .collection("usuarios")
            .doc(user.user.uid);
          ref
            .set(
              {
                nombre: this.state.Nombre,
                telefono: this.state.Telefono,
                photo: "https://hedronmx.com/avatar.jpeg"
              },
              { merge: true }
            )
            .then(docRef => {
              Alert.alert("¡Registrado!", "Gracias por usar nuestra app", [
                { text: "OK", onPress: () => console.log("OK ButtonPressed") }
              ]);
            });
        })
        .catch(error => {
          const { code, message } = error;
          Alert.alert("Error codigo: " + code, message, [
            { text: "OK", onPress: () => console.log("Hwa") }
          ]);
        });
    } else {
      console.log("Wroooong");
      Alert.alert(
        "¡Error!",
        "Asegurate de llenar todos los campos correctamente",
        [{ text: "OK", onPress: () => console.log("Hwa") }]
      );
    }
  };

  renderControlIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={back} />;
  };

  renderLeftControl = (): React.ReactElement<TopNavigationActionProps> => {
    return (
      <TopNavigationAction
        icon={this.renderControlIcon}
        onPress={this.handleBackButton}
      />
    );
  };

  render() {
    const estilo = {
      bottom: this.state.keyboardOffset,
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    };

    return (
      <Layout style={estilo}>
        <TopNavigation
          style={styles.abajo}
          leftControl={this.renderLeftControl()}
        />
        <Image source={Logo} style={styles.logo} resizeMode="center" />
        <Text style={styles.text} category="h1">
          Perritos
        </Text>
        <Input
          style={styles.input}
          value={this.state.Nombre}
          onChangeText={this.onNombre}
          onSubmitEditing={this.validateNombre}
          status={this.state.nombreStatus}
          placeholder="Nombre"
          autoCorrect={false}
        />

        <Input
          style={styles.input}
          value={this.state.Telefono}
          autoCapitalize="none"
          onChangeText={this.onTelefono}
          placeholder="Telefono"
          status={this.state.telefonoStatus}
          caption={this.state.telefonoCaption}
          autoCorrect={false}
          keyboardType="phone-pad"
        />

        <Input
          style={styles.input}
          value={this.state.Correo}
          onChangeText={this.onCorreo}
          autoCapitalize="none"
          placeholder="Correo Electrónico"
          status={this.state.correoStatus}
          caption={this.state.correoCaption}
          keyboardType="email-address"
          autoCorrect={false}
          onFocus={this._keyboardDidShow}
        />
        <Input
          style={styles.input}
          value={this.state.Contrasena}
          onChangeText={this.onContrasena}
          autoCapitalize="none"
          placeholder="Contraseña"
          status={this.state.contrasenaStatus}
          caption={this.state.contrasenaCaption}
          secureTextEntry={true}
          autoCorrect={false}
          onFocus={this._keyboardDidShow}
        />
        <Button
          onPress={this.Register}
          style={styles.boton}
          disabled={!this.state.Contrasena.length}
        >
          Registrarse
        </Button>
        <Text
          onPress={() => {
            this.props.navigation.navigate("Login");
          }}
          style={styles.subtext}
        >
          ¿Ya tienes cuenta? Inicia Sensión
        </Text>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 100
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  abajo: {
    position: "absolute",
    top: 0,
    marginVertical: 32
  },
  back: {
    height: 1
  }
});

