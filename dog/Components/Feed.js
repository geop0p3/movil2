import * as React from "react";
import { StyleSheet, Image, Alert } from "react-native";
import {
  Button,
  Layout,
  Text,
  BottomNavigation,
  BottomNavigationTab,
  BottomNavigationProps
} from "react-native-ui-kitten";
import Logo from "../Images/logo.png";
import Timeline from "./Timeline";
import Reportar from "./Reportar";
import Settings from "./Settings";
import Comunidad from "./Comunidad";
import Tienda from "./Tienda";
import perdidos from "../Images/lupa.png";
import comunidad from "../Images/comunidad.png";
import tienda from "../Images/tienda.png";
import reportar from "../Images/reportar.png";
import config from "../Images/settings.png";
import firebase from "../Firebase";
import {
  createBottomTabNavigator,
  createAppContainer,
  NavigationContainer,
  NavigationContainerProps,
  NavigationRoute
} from "react-navigation";

var index = [];

export class Feed extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      telefono: null
    };
    
    this.ref = firebase.firestore().collection("usuarios").doc(firebase.auth().currentUser.uid);
  }
  
  componentDidMount() {
    this.ref
      .get()
      .then(doc => {
        if (!doc.exists) {
        } else {
          if (!doc.data().telefono){
           Alert.alert("No haz ingresado tu teléfono", "Es necesario que ingreses tu número de teléfono para poder reportar", [
        {
          text: "¡Vamos pues!", 
          onPress: () => {
          this.props.navigation.navigate("Settings")
          this.setState({
            selectedIndex: 2
          });
          }
        },
        {
          text: "No ahora no",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
          }
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
    if (this.props.Comentario != "") {
      this.setState({
        comentario: this.props.Comentario
      });
    }
  }

  perdidosIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={perdidos} />;
  };

  reportarIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={reportar} />;
  };

  configIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={config} />;
  };
  
  tiendaIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={tienda} />;
  };
  
  comunidadIcon = (style: StyleType): React.ReactElement<ImageProps> => {
    return <Image style={style} source={comunidad} />;
  };

  onTabSelect = (selectedIndex: number) => {
    const { index: selectedRoute } = this.props.navigation.state.routes;
    this.setState({ selectedIndex });
    var arreglo = this.props.navigation.state.routes;
    this.props.navigation.navigate(arreglo[selectedIndex].routeName);
  };

  render() {
    return (
      <BottomNavigation
        style={styles.abajo}
        selectedIndex={this.state.selectedIndex}
        onSelect={this.onTabSelect}
      >
        <BottomNavigationTab title="Reportes" icon={this.perdidosIcon} />
        <BottomNavigationTab title="Comunidad" icon={this.comunidadIcon} />
        <BottomNavigationTab title="Reportar" icon={this.reportarIcon} />
        <BottomNavigationTab title="Tienda" icon={this.tiendaIcon} />
        <BottomNavigationTab title="Yo" icon={this.configIcon} />
      </BottomNavigation>
    );
  }
}

const Bottomnavigator: NavigationContainer = createBottomTabNavigator(
  {
    Timeline: { screen: Timeline },
    Comunidad: {screen: Comunidad},
    Reportar: { screen: Reportar },
    Tienda: {screen: Tienda},
    Settings: { screen: Settings }
  },
  {
    initialRouteName: "Timeline",
    tabBarComponent: Feed
  }
);

const AppContainer = createAppContainer(Bottomnavigator);

export default AppContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  abajo: {
    position: "absolute",
    bottom: 0
  }
});

