import * as React from "react";
import { StyleSheet, Image, TouchableHighlight, Alert } from "react-native";
import {
  Button,
  Layout,
  Text,
  Input,
  Avatar,
  AvatarProps
} from "react-native-ui-kitten";
import Logo from "../Images/logo.png";
import Goog from "../Images/google.png";
import Face from "../Images/face.png";
import firebase from "../Firebase";
import { AccessToken, LoginManager } from "react-native-fbsdk";
import * as Facebook from "expo-facebook";
import * as GoogleSignIn from "expo-google-sign-in";
import { Google } from "expo";
import * as Expo from "expo";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Correo: "",
      Contrasena: "",
      correoStatus: "",
      correoCaption: "",
      contrasenaStatus: "",
      contrasenaCaption: ""
    };
  }

  // Web version. Use as fall back
  //
  // GoogleLogin2 = async () => {
  //   try {
  //   const result = await Expo.Google.logInAsync({
  //     androidClientId: '556431626098-l9sti0tbv3f3ujtf8eujnu4o544q3p00.apps.googleusercontent.com',
  //     // iosClientId: '556431626098-u9ts3pq1nf047jfm90mbi3lfqqca628i.apps.googleusercontent.com',
  //     scopes: ['profile', 'email'],
  //   });

  //   if (result.type === 'success') {
  //     const credential = firebase.auth.GoogleAuthProvider.credential(
  //         result.idToken
  //       );
  //       firebase
  //         .auth()
  //         .signInWithCredential(credential)
  //         .then(function(result) {
  //         });
  //   } else {
  //     return { cancelled: true };
  //   }
  // } catch (e) {
  //   return { error: true };
  // }
  // }

  GoogleLogin = async () => {
    try {
      await GoogleSignIn.initAsync({
        clientId:
          "556431626098-f6fum8egudvk1ro04mmnu4m2uic5vjg8.apps.googleusercontent.com"
      });
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      console.log(type);
      if (type === "success") {
        console.log("User HERE -----------", user);
        const credential = firebase.auth.GoogleAuthProvider.credential(
          user.auth.idToken
        );
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(function(user) {
            console.log("This is google ", user);
            firebase
              .firestore()
              .collection("usuarios")
              .doc(firebase.auth().currentUser.uid)
              .set(
                {
                  nombre: firebase.auth().currentUser.displayName,
                  photo: firebase.auth().currentUser.photoURL
                },
                { merge: true }
              );
          });
      } else {
        console.log("cancelled");
        Alert.alert("¡Error!", "Cancelaste el proceso", [
          { text: "OK", onPress: () => console.log("Hwa") }
        ]);
      }
    } catch (e) {
      console.log(e);
      const { code, message } = e;
      console.log(message);
      Alert.alert("Error codigo: " + code, message, [
        { text: "OK", onPress: () => console.log("Hwa") }
      ]);
    }
  };

  FacebookLogin = async () => {
    console.log("Entro");
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      "2410214365736416",
      {
        permission: "public_profile"
      }
    );
    if (type == "success") {
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      let actualname = null;
      let actualphoto = null;
      firebase
        .auth()
        .signInWithCredential(credential)
        .then(user => {
          firebase
            .firestore()
            .collection("usuarios")
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then(doc => {
              if (!doc.exists) {
                firebase
                  .firestore()
                  .collection("usuarios")
                  .doc(firebase.auth().currentUser.uid)
                  .set(
                    {
                      nombre: firebase.auth().currentUser.displayName,
                      photo:
                        firebase.auth().currentUser.photoURL + "?type=large"
                    },
                    { merge: true }
                  );

                firebase.auth().currentUser.updateProfile({
                  photoURL: firebase.auth().currentUser.photoURL + "?type=large"
                });
              } else {
              }
            })
            .catch(err => {});
        })
        .catch(error => {
          console.log(error);
          const { code, message } = error;
          Alert.alert("Error codigo: " + code, message, [
            { text: "OK", onPress: () => console.log("Hwa") }
          ]);
        });
    }
  };

  onCorreo = (Correo: string) => {
    this.setState({ Correo });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(Correo) === true) {
      this.setState({ correoStatus: "success" });
      this.setState({ correoCaption: "" });
    } else {
      this.setState({ correoStatus: "danger" });
      this.setState({ correoCaption: "Correo electrónico incorrecto" });
    }
  };

  onContrasena = (Contrasena: string) => {
    this.setState({ Contrasena });
    if (Contrasena.length >= 6) {
      this.setState({ contrasenaStatus: "success" });
      this.setState({ contrasenaCaption: "" });
    } else {
      this.setState({ contrasenaStatus: "danger" });
      this.setState({
        contrasenaCaption: "La contraseña debe contener al menos 6 caracteres"
      });
    }
  };

  Login = () => {
    console.log("Entro");
    if (
      this.state.contrasenaStatus == "success" &&
      this.state.correoStatus == "success"
    ) {
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.Correo, this.state.Contrasena)
        .then(user => {
          this.props.navigation.navigate("Feed");
        })
        .catch(error => {
          const { code, message } = error;
          console.log(code);
          console.log(message);
          Alert.alert("Error codigo: " + code, message, [
            { text: "OK", onPress: () => console.log("Hwa") }
          ]);
        });
    } else {
      Alert.alert("¡Error!", "Asegurate de llenar los campos correctamente", [
        { text: "OK", onPress: () => console.log("Hwa") }
      ]);
    }
  };

  render() {
    return (
      <Layout style={styles.container}>
        <Image source={Logo} style={styles.logo} resizeMode="center" />

        <Text style={styles.text} category="h1">
          Perritos
        </Text>

        <Input
          style={styles.input}
          value={this.state.Correo}
          onChangeText={this.onCorreo}
          autoCapitalize="none"
          keyboardType="email-address"
          autoCorrect={false}
          placeholder="Correo Electronico"
          status={this.state.correoStatus}
          caption={this.state.correoCaption}
        />

        <Input
          style={styles.input}
          value={this.state.Contrasena}
          onChangeText={this.onContrasena}
          placeholder="Contraseña"
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry={true}
          keyboardType="default"
          status={this.state.contrasenaStatus}
          caption={this.state.contrasenaCaption}
        />

        <Button
          title="Ingresar"
          disabled={!this.state.Contrasena.length}
          onPress={this.Login}
          style={styles.boton}
        >
          Ingresar
        </Button>

        <Button
          status="danger"
          onPress={() => {
            this.props.navigation.navigate("Register");
          }}
          style={styles.boton}
        >
          Registrarse
        </Button>

        <Text style={styles.textito}>Ingresa Con Tus Redes Sociales</Text>
        <Layout style={styles.container2}>
          <TouchableHighlight onPress={this.GoogleLogin} underlayColor="white">
            <Avatar style={styles.google} source={Goog} />
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.FacebookLogin}
            underlayColor="white"
          >
            <Avatar style={styles.google} source={Face} />
          </TouchableHighlight>
        </Layout>

        <Text
          onPress={() => {
            this.props.navigation.navigate("Recover");
          }}
          style={styles.subtext}
        >
          Olvide mi contraseña
        </Text>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  container2: {
    flexDirection: "row",
    marginVertical: 8
  },
  text: {
    marginVertical: 8
  },
  subtext: {
    color: "lightgray",
    marginVertical: 32,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 100
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  textito: {
    marginHorizontal: 16,
    textAlign: "center",
    marginTop: 16
  },
  google: {
    marginHorizontal: 12,
    marginVertical: 8
  }
});

