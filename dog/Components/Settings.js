import * as React from "react";
import {
  StyleSheet,
  Image,
  Alert,
  Keyboard,
  Dimensions,
  TouchableHighlight,
  ScrollView
} from "react-native";
import { Button, Layout, Text, Input } from "react-native-ui-kitten";
import firebase from "../Firebase";
import Icon from "../Images/settings.png";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

export default class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Nombre: firebase.auth().currentUser.displayName,
      Correo: firebase.auth().currentUser.email,
      profilePicture: "http://hedronmx.com/avatar.jpeg",
      Contrasena: "",
      nombreStatus: "",
      Telefono: "",
      telefonoStatus: "",
      telefonoCaption: "",
      correoStatus: "",
      correoCaption: "",
      contrasenaStatus: "",
      contrasenaCaption: "",
      keyboardOffset: 0,
      photo: null,
      currentPassword: "",
      currentPasswordStatus: "info",
      currentPasswordCaption: ""
    };
      this.ref = firebase
      .firestore()
      .collection("usuarios")
      .doc(firebase.auth().currentUser.uid);
  }

  Logout = () => {
    firebase
      .auth()
      .signOut()
      .then(function() {
        // Sign-out successful.
      })
      .catch(function(error) {
        // An error happened.
      });
  };

  componentDidMount() {
    if (firebase.auth().currentUser.photoURL != null) {
      this.setState({ profilePicture: firebase.auth().currentUser.photoURL });
    }
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    this.state.screenHeight = Dimensions.get("window");
    
    this.ref
      .get()
      .then(doc => {
        if (!doc.exists) {
        } else {
          if (doc.data().telefono){
            this.setState({
              Telefono: doc.data().telefono
            })
          }
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
    
  }
  
  onContentSizeChange = (contentWidth, contentHeight) => {
    // Save the content height in state
    this.setState({ screenHeight: contentHeight });
  };

  componentWillUnmount() {
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = event => {
    const screenHeight = Math.round(Dimensions.get("window").height);
    this.setState({
      keyboardOffset: screenHeight * 0.2
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      keyboardOffset: 0
    });
  };

  Guardar = () => {
    if (
      this.state.nombreStatus == "success" ||
      this.state.contrasenaStatus == "success" ||
      this.state.correoStatus == "success" ||
      this.state.telefonoStatus =="success"
    ) {
      if (this.state.nombreStatus == "success") {
        console.log("entro nombre");
        firebase.auth().currentUser.updateProfile({
          displayName: this.state.Nombre
        });
        this.ref
          .set(
            {
              nombre: this.state.Nombre
            },
            { merge: true }
          )
          .then(docRef => {
            Alert.alert("¡Hecho!", "Se actualizo tu nombre", [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      }
      
      if (this.state.telefonoStatus == "success") {
        console.log("entro telefono");
        this.ref
          .set(
            {
              telefono: this.state.Telefono
            },
            { merge: true }
          )
          .then(docRef => {
            Alert.alert("¡Hecho!", "Se actualizo tu teléfono", [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      }

      if (this.state.contrasenaStatus == "success") {
        this.reauthenticate(this.state.currentPassword)
          .then(() => {
            var user = firebase.auth().currentUser;
            user
              .updatePassword(this.state.Contrasena)
              .then(() => {
                this.setState({
                  Contrasena: "",
                  currentPassword: "",
                  currentPasswordStatus: "info"
                });
                Alert.alert("¡Hecho!", "Se actualizo tu contraseña", [
                  { text: "OK", onPress: () => console.log("Hwa") }
                ]);
              })
              .catch(error => {
                console.log(error);
                const { code, message } = error;
                Alert.alert("¡Error!", message, [
                  { text: "OK", onPress: () => console.log("Hwa") }
                ]);
              });
          })
          .catch(error => {
            const { code, message } = error;
            Alert.alert("¡Error!", message, [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      }

      if (this.state.correoStatus == "success") {
        this.reauthenticate(this.state.currentPassword)
          .then(() => {
            var user = firebase.auth().currentUser;
            user
              .updateEmail(this.state.Correo)
              .then(() => {
                console.log("Email updated!");
                this.setState({
                  Correo: "",
                  currentPassword: "",
                  currentPasswordStatus: "info",
                  Contrasena: ""
                });
                Alert.alert("¡Hecho!", "Se actualizo tu correo", [
                  { text: "OK", onPress: () => console.log("Hwa") }
                ]);
              })
              .catch(error => {
                const { code, message } = error;
                Alert.alert("¡Error!", message, [
                  { text: "OK", onPress: () => console.log("Hwa") }
                ]);
              });
          })
          .catch(error => {
            const { code, message } = error;
            Alert.alert("¡Error!", message, [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });
      }
    } else {
      Alert.alert(
        "¡Error!",
        "Asegurate de llenar los campos correctamente, para cambiar el correo es necesario agregar tu contraseña actual",
        [{ text: "OK", onPress: () => console.log("Hwa") }]
      );
    }
  };

  reauthenticate = currentPassword => {
    var user = firebase.auth().currentUser;
    var cred = firebase.auth.EmailAuthProvider.credential(
      user.email,
      currentPassword
    );
    return user.reauthenticateWithCredential(cred);
  };

  onNombre = (Nombre: string) => {
    this.setState({ Nombre });
    this.setState({ nombreStatus: "success" });
  };
  
  onTelefono = (Telefono: string) => {
    this.setState({ Telefono });
    let reg = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (reg.test(Telefono) === true) {
      this.setState({ telefonoStatus: "success" });
      this.setState({ telefonoCaption: "" });
    } else {
      this.setState({ telefonoStatus: "danger" });
      this.setState({ telefonoCaption: "Teléfono inválido" });
    }
  };

  onCorreo = (Correo: string) => {
    this.setState({ Correo });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(Correo) === true) {
      this.setState({ correoStatus: "success" });
      this.setState({ correoCaption: "" });
    } else {
      this.setState({ correoStatus: "danger" });
      this.setState({ correoCaption: "Correo electrónico incorrecto" });
    }
  };

  onContrasena = (Contrasena: string) => {
    this.setState({ Contrasena });
    if (Contrasena.length >= 6) {
      this.setState({ contrasenaStatus: "success" });
      this.setState({ contrasenaCaption: "" });
    } else {
      this.setState({ contrasenaStatus: "danger" });
      this.setState({
        contrasenaCaption: "La contraseña debe contener al menos 6 caracteres"
      });
    }
  };

  oncurrentPassword = (currentPassword: string) => {
    this.setState({ currentPassword });
    if (currentPassword.length >= 6) {
      this.setState({ currentPasswordStatus: "success" });
      this.setState({ currentPasswordCaption: "" });
    } else {
      this.setState({ currentPasswordStatus: "danger" });
      this.setState({
        currentPasswordCaption:
          "La contraseña debe contener al menos 6 caracteres"
      });
    }
  };

  async checkCameraRollPermission() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      Alert.alert("Hey", "Necesitamos permiso para acceder a tus fotos", [
        {
          text: "Abrir Configuración",
          onPress: () => Linking.openURL("app-settings:")
        },
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]);
      this.setState({
        hasCameraRollPermissions: false
      });
      return false;
    }
    this.setState({
      hasCameraRollPermissions: true
    });
    return true;
  }

  Photo = async () => {
    const checkPermissions = await this.checkCameraRollPermission();

    if (!checkPermissions) return;

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1]
    });

    if (!result.cancelled) {
      const imageName =
        "Photo" +
        Math.floor(Math.random() * 1000) +
        1 +
        Math.floor(Math.random() * 1000) +
        1;
      this.setState({
        profilePicture: firebase.auth().currentUser.photoURL,
        photo: result.uri
      });
      this.uploadImage(this.state.photo, imageName);
    }
  };

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase
      .storage()
      .ref()
      .child(firebase.auth().currentUser.uid + "/" + imageName);

    ref.put(blob).then(result => {
      ref.getDownloadURL().then(result => {
        console.log(result);

        firebase.auth().currentUser.updateProfile({
          photoURL: result
        });
        this.ref
          .set(
            {
              photo: result
            },
            { merge: true }
          )
          .then(docRef => {
            Alert.alert("¡Hecho!", "Se actualizo tu fotografia", [
              { text: "OK", onPress: () => console.log("Hwa") }
            ]);
          });

        this.setState({ profilePicture: result });
      });
    });
  };

  render() {
    const estilo = {
      bottom: this.state.keyboardOffset,
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    };

    return (
    <ScrollView
        style={styles.scroll}
        onContentSizeChange={this.onContentSizeChange}
      >
      <Layout style={estilo}>
        <Text style={styles.text} category="h5">
          Mi Perfil
        </Text>
        <TouchableHighlight onPress={this.Photo} underlayColor="white">
          <Image
            source={{ uri: this.state.profilePicture }}
            style={{
              width: 120,
              height: 120,
              borderRadius: 120 / 2,
              marginVertical: 8
            }}
          />
        </TouchableHighlight>
        <Input
          style={styles.input}
          value={this.state.Nombre}
          onChangeText={this.onNombre}
          onSubmitEditing={this.validateNombre}
          status={this.state.nombreStatus}
          placeholder="Nombre"
          autoCorrect={false}
        />
        
        <Input
          style={styles.input}
          value={this.state.Telefono}
          onChangeText={this.onTelefono}
          status={this.state.telefonoStatus}
          caption={this.state.telefonoCaption}
          placeholder="Telefono"
          autoCorrect={false}
        />

        <Input
          style={styles.input}
          value={this.state.Correo}
          onChangeText={this.onCorreo}
          autoCapitalize="none"
          placeholder="Correo Electrónico"
          status={this.state.correoStatus}
          caption={this.state.correoCaption}
          keyboardType="email-address"
          autoCorrect={false}
          onFocus={this._keyboardDidShow}
        />
        <Input
          style={styles.input}
          value={this.state.currentPassword}
          onChangeText={this.oncurrentPassword}
          autoCapitalize="none"
          placeholder="Contraseña Actual"
          status={this.state.currentPasswordStatus}
          caption={this.state.currentPasswordCaption}
          secureTextEntry={true}
          autoCorrect={false}
          onFocus={this._keyboardDidShow}
        />
        <Input
          style={styles.input}
          value={this.state.Contrasena}
          onChangeText={this.onContrasena}
          autoCapitalize="none"
          placeholder="Nueva Contraseña"
          status={this.state.contrasenaStatus}
          caption={this.state.contrasenaCaption}
          secureTextEntry={true}
          autoCorrect={false}
          onFocus={this._keyboardDidShow}
        />
        <Button
          onPress={this.Guardar}
          disabled={!this.state.currentPassword.length}
          style={styles.boton}
        >
          Guardar
        </Button>
        <Button onPress={this.Logout} style={styles.boton} status="danger">
          Cerrar Sesión
        </Button>
      </Layout>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 6
  },
  subtext: {
    color: "lightgray",
    marginVertical: 8,
    fontSize: 12,
    fontWeight: "600"
  },
  logo: {
    height: 100
  },
  boton: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  input: {
    alignSelf: "stretch",
    marginHorizontal: 8,
    marginVertical: 8
  },
  scroll: {
    flexGrow: 1,
    marginVertical: 64
  },
});

