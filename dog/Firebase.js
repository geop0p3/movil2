import * as firebase from "firebase";
import firestore from "firebase/firestore";

const settings = {};

const config = {
  apiKey: " AIzaSyD2cf7upNvh9a4R0cgrZQlQ4RX-s9NKgkE",
  authDomain: "perritos-19021.firebaseapp.com",
  databaseURL: "https://perritos-19021.firebaseio.com",
  projectId: "perritos-19021",
  storageBucket: "perritos-19021.appspot.com",
  messagingSenderId: "556431626098"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;

