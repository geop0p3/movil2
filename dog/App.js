import * as React from "react";
import { mapping, light as lightTheme } from "@eva-design/eva";
import { ApplicationProvider } from "react-native-ui-kitten";
import Feed from "./Components/Feed";
import Login from "./Components/Login";
import Register from "./Components/Register";
import Recover from "./Components/Recover";
import Loading from "./Components/Loading";
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { createSwitchNavigator, createAppContainer } from "react-navigation";
import firebase from "./Firebase";
import { AccessToken, LoginManager } from "react-native-fbsdk";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      user: null
    };
  }

  componentDidMount() {
    this.authSubscription = firebase.auth().onAuthStateChanged(user => {
      this.setState({
        loading: false,
        user
      });
    });
  }

  componentWillUnmount() {
    this.authSubscription();
  }

  render() {
    if (this.state.loading == true) {
      return (
        <ApplicationProvider mapping={mapping} theme={lightTheme}>
          <Loading />
        </ApplicationProvider>
      );
    }

    if (this.state.user) {
      return (
        <ApplicationProvider mapping={mapping} theme={lightTheme} >
          <Feed />
        </ApplicationProvider>
      );
    }

    return (
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <AppContainer />
      </ApplicationProvider>
    );
  }
}

const AppSwitchNavigator = createSwitchNavigator({
  Login: { screen: Login },
  Feed: { screen: Feed },
  Register: { screen: Register },
  Recover: { screen: Recover }
});

const AppContainer = createAppContainer(AppSwitchNavigator);

