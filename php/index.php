<!doctype html>
<html>
 
<head>
 
<title>Ejemplo sencillo de AJAX</title>

<style>
#btnseccion{ color:#F90409; cursor:pointer;}

</style>
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
<script>
function realizaProceso(valorCaja1, valorCaja2){
        var parametros = {
                "valorCaja1" : valorCaja1,
                "valorCaja2" : valorCaja2
        };
        $.ajax({
                data:  parametros,
                url:   'procesophp.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
                }
        });
}
</script>


 
</head>
 
<body>
 
Introduce valor 1
 
<input type="text" name="caja_texto" id="valor1" value="0"/> 
 
 
Introduce valor 2
 
<input type="text" name="caja_texto" id="valor2" value="0"/>
 
Realiza suma
 
<input type="button" href="javascript:;" onclick="realizaProceso($('#valor1').val(), $('#valor2').val());return false;" value="Calcula"/>
 
<br/>
 
Resultado: <span id="resultado">0</span>



<br/><br/><br/><br/><br/>

Introduce valor de seccion
 
<input type="text" name="seccion" id="seccion" value="1"/> 
<p id="btnseccion">click aqui para ver seccion</p>

<p id="resultadobd">0</p>
<p id="resultadobd1">0</p>
<p id="resultadobd2">0</p>

<script>
$(document).ready(function(){
    $("#btnseccion").click(function(){
       $("#resultadobd").html(" ");
	   $("#resultadobd1").html(" ");
	   $("#resultadobd2").html(" ");
	   var parametros = {
                "seccion" : $("#seccion").val()
        };
        $.ajax({
                data:  parametros,
                url:   'procesobd.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultadobd").html("Procesando, espere por favor...");
                },
                success:  function (response) {
						var json =response;
						//var obj = JSON.parse(json);
						console.log(json.success);
							
						  
                        $("#resultadobd").html(json.success);
						$("#resultadobd1").html(json.data.message);
						$("#resultadobd2").html('tiene un id: '+json.data.datos[0].id+', el valor es: '+json.data.datos[0].valor+', es de tipo: '+json.data.datos[0].tipo);
                }
        });
	   
	   
    });
});
</script>
 
</body>
 
</html>